# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  num = (1..100).to_a.sample
  guess_count = 0
  guesses = []
  while true
    guess_count += 1
    puts "guess a number"
    guess = gets
    puts guess.chomp
    if guess.chomp.to_i < num
      puts "too low"
    elsif guess.chomp.to_i > num
      puts "too high"
    elsif guess == "0\n"
      puts "NoMoreInput"
    elsif guess.chomp.to_i > 100
      puts "must be less than 100"
    else
      break
    end
  end
  puts guess_count
end

#Write file_shuffler program that:
#* prompts the user for a file name
#* reads that file
#* shuffles the lines
#* saves it to the file "{input_name}-shuffled.txt".

def file_shuffler
  puts "Please enter in a file name."
  file1 = gets.chomp
  contents = File.readlines(file1).shuffle
  file2 = File.open("shuffled_#{file1}", "w")
  contents.each do |line|
    file2.write(line)
  end
  file2.close
end
